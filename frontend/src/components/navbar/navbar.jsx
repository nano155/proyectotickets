import NavbarStyled from "./navbar.styled";

const Navbar = () => {
  return (
    <NavbarStyled position="static">
      App de tickets
    </NavbarStyled>
  );
}

export default Navbar;
