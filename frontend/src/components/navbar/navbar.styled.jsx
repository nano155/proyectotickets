import styled from "styled-components";
import AppBar from '@mui/material/AppBar';

const NavbarStyled = styled(AppBar)`
  padding: 1.5rem;
`;

export default NavbarStyled;
