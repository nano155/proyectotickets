import styled from "styled-components";

const WrapperListTikect = styled.div`
  margin: 2rem 4rem;

  .row-title {
    display: flex;
    justify-content: space-between;
    margin-bottom: 1.5rem;
  }
`;

export default WrapperListTikect;
