import * as React from 'react';
import Box from '@mui/material/Box';
import { DataGrid } from '@mui/x-data-grid';
import { Button, IconButton, Typography, Dialog, DialogTitle, DialogContent, DialogActions } from '@mui/material';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import WrapperListTikect from './wrapper-list-tikect.styled';

const rows = [
  { id: 1, hour: new Date().getHours(), date: new Date().getDate(), state: 35 },
  { id: 2, hour: new Date().getHours(), date: new Date().getDate(), state: 35 },
  { id: 3, hour: new Date().getHours(), date: new Date().getDate(), state: 35 },
  { id: 4, hour: new Date().getHours(), date: new Date().getDate(), state: 35 },
];

const getFields = (handleOnClick) => {
  return [
    { field: 'id', headerName: 'ID', width: 90 },
    {
      field: 'hour',
      headerName: 'Hora',
      width: 150,
    },
    {
      field: 'date',
      headerName: 'Fecha',
      width: 150,
    },
    {
      field: 'state',
      headerName: 'Estado',
      type: 'string',
      width: 150,
    },
    {
      field: "action",
      headerName: "Acciones",
      sortable: false,
      renderCell: (params) => {
        return (
          <>
            <IconButton onClick={handleOnClick(params.id, 'edit')} color="primary" component="label">
              <EditIcon />
            </IconButton>
            <IconButton onClick={handleOnClick(params.id, 'delete')} color="primary" component="label">
              <DeleteIcon />
            </IconButton>
          </>
        );
      }
    },
  ];
} 

const ListTikect = () => {
  const [openModal, setOpenModal] = React.useState(false);
  const [isEdit, setIsEdit] = React.useState(false);


  const handleModal = () => {
    setOpenModal(!openModal)
    if (isEdit) setIsEdit(false)
  }

  const handleOnClick = (id, type) => () => {
    if (type === 'edit') {
      setIsEdit(true)
      handleModal()
      return;
    } 
    console.log(id, type)
  }

  const columns = getFields(handleOnClick);

  return (
    <WrapperListTikect>
      <div className='row-title'>
        <Typography variant="h5">
          Lista de tickets
        </Typography>
        <Button onClick={handleModal} variant="contained">Agregar nuevo</Button>
      </div>
      <Box>
        <DataGrid
          rows={rows}
          columns={columns}
          pageSize={5}
          rowsPerPageOptions={[5]}
          autoHeight
          hideFooterSelectedRowCount
        />
      </Box>

      <form>
        <Dialog
          open={openModal}
          onClose={handleModal}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">
            {isEdit ? 'Editar' : 'Crear'}
          </DialogTitle>
          <DialogContent>
            Funciona ?
          </DialogContent>
          <DialogActions>
            <Button onClick={handleModal}>Salir</Button>
            <Button>Guardar</Button>
          </DialogActions>
        </Dialog>
      </form>
    </WrapperListTikect>
  );
}




export default ListTikect;