import Navbar from "./components/navbar/navbar";
import ListTikect from "./components/list-tikect/list-tikect";


function App() {
  return (
    <>
      <Navbar />
      <ListTikect />
    </>
  );
}

export default App;
