import { app } from "./src/app.js";
import { env } from "./src/config.js";
import { startDatabase } from "./src/database.js";

const startApp = async () => {
    try {
        const statusDB = await startDatabase();
        if (!statusDB) return;
        app.listen(env.PORT, () => {
            console.log(`[server]: Server is running at https://localhost:${env.PORT}`);
        });
    } catch (error) {
        console.log(error);
    }
};

startApp();