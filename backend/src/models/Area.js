import mongoose, { model } from "mongoose";
const { Schema } = mongoose;

const areaSchema = new Schema({
  id: Number, 
  name: String
});

export const AreaModel = model("Area", areaSchema);
