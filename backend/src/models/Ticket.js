import mongoose from "mongoose";

const { Schema } = mongoose;

const ticketSchema = new Schema({
  idTicket: Number,
  fechaTicket: String,
  asuntoTicket: String,
  descripcionTicket: String,
  numeroTicket: Number,
  estadoTicket: Number,
});

export const TicketModel = mongoose.model("Ticket", ticketSchema);
