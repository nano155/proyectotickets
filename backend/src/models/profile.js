import mongoose from "mongoose";
import bcrypt from "bcrypt";

const { Schema } = mongoose; // js puro entonces se usa const require remplazando export

const profileSchema = new Schema({
  name: { type: String, unique: true },
  lastName: { type: String, unique: true },
  birthday: { type: String, unique: true },
  email: {
    type: String,
    require: [true, "Email requerido"],
    lowercase: true,
    unique: true,
  },
  password: {
    type: String,
  },
  userType: {
    type: String,
    enum: ["user", "admin"],
    default: "user",
  },
});
profileSchema.pre("save", async function (next) {
  const salt = await bcrypt.genSalt(10);
  this.password = await bcrypt.hash(this.password, salt);
});

profileSchema.methods.comprobarClave = async function (claveFormulario) {
  return await bcrypt.compare(claveFormulario, this.password);
};

const ProfileModel = mongoose.model("profile", profileSchema); //

export default ProfileModel;
