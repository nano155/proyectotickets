import express from "express";
import helmet from "helmet";
import bodyParser from "body-parser";
import compression from "compression";
import cors from "cors";
import { profilesController } from "./controllers/profiles.controller.js";
import { areasController } from "./controllers/areas.controller.js";
import { ticketController } from "./controllers/ticket.controller.js";


export const app = express();

// Set security Http headers
app.use(helmet());
// Parse json request body
app.use(bodyParser.json());
// parse urlencoded request body
app.use(bodyParser.urlencoded({extended: false}));
// gzip compression
app.use(compression());
// Enable cors
app.use(cors());

// Add controllers
profilesController(app);
areasController(app);
ticketController(app);
