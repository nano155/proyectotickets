import { AreaModel } from "../models/area.js";

export const createArea = async (args) => {
  const { id, name } = args;
  if (!id) throw new Error("the id is mising");
  if (!name) throw new Error("the name is mising");
  const crear = new AreaModel({ id, name });
  await crear.save();
  return crear;
};

export const getAllArea = async () => {
  const Areas = await AreaModel.find();
  return Areas;
};
