import ProfileModel from "../models/profile.js";
import generarJWT from "../lib/jwt.js";

export const createProfile = async (args) => {
  const { name, lastName, birthday, email, password, usertype } = args;
  if (!name) throw new Error("Hace falta el name");
  if (!lastName) throw new Error("Hace falta el apellido");
  if (!birthday) throw new Error("Hace falta tu fecha de nacimiento");
  if (!email) throw new Error("Hace falta un correo");
  if (!password) throw new Error("Hace falta una contraseña");
  const create = new ProfileModel({
    name,
    lastName,
    birthday,
    email,
    password,
    usertype,
  });
  await create.save();
  return create;
};
export const eliminar = async (req, res) => {
  console.log("respondiedo desde el metodo eliminar");
};
export const autenticar = async (req, res) => {
  const { email, password } = req.body;

  //comprobar si el usuario existe
  const profile = await ProfileModel.findOne({ email });
  if (!profile) {
    const error = new Error("El profile no existe.");
    return res.status(404).json({ msg: error.message });
  }

  //comprobar la contraseña
  if (await profile.password(password)) {
    res.json({
      _id: profile._id,
      name: profile.name,
      email: profile.email,
      tokenJwt: generarJWT(profile._id),
    });
  } else {
    const error = new Error("La clave es incorrecta.");
    res.json({ msg: error.message });
  }
  1;
};

export const getAllProfile = async () => {
  const Profiles = await ProfileModel.find();
  return Profiles;
};
