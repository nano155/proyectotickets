import { TicketModel } from "../models/Ticket.js";

export const createTicket = async (args) => {
  const {
    idTicket,
    fechaTicket,
    asuntoTicket,
    descripcionTicket,
    numeroTicket,
    estadoTicket,
  } = args;
  if (!idTicket) throw new Error("Hace falta una una id del ticket");
  if (!fechaTicket) throw new Error("Hace falta la fecha de ticket");
  if (!asuntoTicket) throw new Error("Hace falta el asunto del ticket");
  if (!descripcionTicket)
    throw new Error("Hace falta una descripcion del ticket");
  if (!numeroTicket) throw new Error("Hace falta un numero de ticket");
  if (!estadoTicket) throw new Error("Hace falta un estado del ticket");
  const create = new TicketModel({
    idTicket,
    fechaTicket,
    asuntoTicket,
    descripcionTicket,
    numeroTicket,
    estadoTicket,
  });
  await create.save();
  return create;
};

export const getAllTicket = async () => {
  const Ticket = await TicketModel.find();
  return Ticket;
};
