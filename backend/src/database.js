import mongoose from "mongoose";
import { env } from "./config.js";

const statusDatabase = {
    disconnected: 0,
    connected: 1,
    connecting: 2,
    disconnecting: 3,
}

export const startDatabase = async () => {
    const db = await mongoose.connect(env.HOST_DATABASE);
    if (db.connection.readyState === statusDatabase.connected) {
        console.log(`Succesfull connected database in ${env.HOST_DATABASE}`);
        return true
    };
    console.log('Connection failed database');
    return false;
}
