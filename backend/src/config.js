import { config } from "dotenv";
import { join, dirname } from "path";
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);

const configEnv = {
  path: join(dirname(__filename), "../.env"),
};

config(configEnv); 

const PORT = +(process.env.PORT || 3000);
const HOST_DATABASE = process.env.HOST_DATABASE || "";

export const env = {
  PORT,
  HOST_DATABASE
}