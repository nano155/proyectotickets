import {
  getAllProfile,
  createProfile,
  eliminar,
  autenticar,
} from "../services/profiles.service.js";

export const profilesController = (app) => {
  app.get("/profiles", async (_req, res) => {
    try {
      const profiles = await getAllProfile();
      return res.status(200).send({
        data: profiles,
      });
    } catch (error) {
      return res.status(400).send({ msg: error.message });
    }
  });
  app.post("/profiles", async (req, res) => {
    try {
      const profile = await createProfile(req.body);
      return res.status(201).send({
        data: profile,
      });
    } catch (error) {
      return res.status(400).send({ msg: error.message });
    }
  });
  app.delete("/profiles", async (_req, res) => {
    try {
      const profiles = await eliminar();
      return res.status(200).send({
        data: profiles,
      });
    } catch (error) {
      return res.status(400).send({ msg: error.message });
    }
  });
  app.post("/login", async (_req, res) => {
    try {
      const profiles = await autenticar();
      return res.status(200).send({
        data: profiles,
      });
    } catch (error) {
      return res.status(400).send({ msg: error.message });
    }
  });
};
