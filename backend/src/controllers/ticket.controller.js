import { TicketModel } from "../models/Ticket.js";
import { createTicket, getAllTicket } from "../services/tickets.service.js";

export const ticketController = (app) => {
  app.get("/tickets/:id", async (_req, res) => {
    const { id } = _req.params;
    const ticket = await TicketModel.findOne({ _id: id });

    if (!ticket || ticket.length == 0) {
      res.status(404).json({ message: "Ticket no encontrado..." });
    } else {
      res.status(302).json(ticket);
    }
  });
  app.get("/tickets", async (_req, res) => {
    try {
      const tickets = await getAllTicket();
      return res.status(200).send({
        data: tickets,
      });
    } catch (error) {
      return res.status(400).send({ msg: error.message });
    }
  });
  app.post("/tickets", async (req, res) => {
    try {
      const ticket = await createTicket(req.body);
      return res.status(201).send({
        data: ticket,
      });
    } catch (error) {
      return res.status(400).send({ msg: error.message });
    }
  });

  app.put("/tickets/:id", async (req, res) => {
    try {
      const { id } = req.params;
      const {
        numeroTicket,
        estadoTicket,
        descripcionTicket,
        asuntoTicket,
        fechaTicket,
      } = req.body;
      const ticket = await TicketModel.findOne({ _id: id });
      if (!ticket) return res.status(400).send({ msg: "error" });
      ticket.overwrite({
        numeroTicket: numeroTicket,
        estadoTicket: estadoTicket,
        descripcionTicket: descripcionTicket,
        asuntoTicket: asuntoTicket,
        fechaTicket: fechaTicket,
      });
      await ticket.save();
      res.status(200).send({ data: ticket });
    } catch (error) {
      return res.status(400).send({ msg: error.message });
    }
  });

  app.delete("/tickets/:id", async (req, res) => {
    const { id } = req.params;
    const ticket = await TicketModel.findOne({ _id: id });
    if (ticket || ticket.length == 0) {
      const response = await ticket.deleteOne({ _id: id });
      res.status(202).json(response);
    } else {
      res.status(404).json({ message: `Ticket no encontrado...` });
    }
  });
};
