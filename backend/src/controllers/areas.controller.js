import { AreaModel } from "../models/area.js";
import { getAllArea, createArea } from "../services/areas.service.js";

export const areasController = (app) => {
  app.get("/areas/:id", async (req, res) => {
    const { id } = req.params;
    const area = await AreaModel.findOne({ _id: id });

    if (!area || area.length == 0) {
      res.status(404).json({ message: "user not found" });
    } else {
      res.status(302).json(area);
    }
  });
  app.get("/areas", async (_req, res) => {
    try {
      const areas = await getAllArea();
      return res.status(200).send({
        data: areas,
      });
    } catch (error) {
      return res.status(400).send({ msg: error.message });
    }
  });
  app.post("/areas", async (req, res) => {
    try {
      const area = await createArea(req.body);
      return res.status(201).send({
        data: area,
      });
    } catch (error) {
      return res.status(400).send({ msg: error.message });
    }
  });

  app.put("/areas/:id", async (req, res) => {
    try {
      const { id } = req.params;
      const { name } = req.body;
      const area = await AreaModel.findOne({ _id: id });
      if (!area) return res.status(400).send({ msg: "error" });
      area.overwrite({
        name: name,
      });
      await area.save();
      res.status(200).send({ data: area });
    } catch (error) {
      return res.status(400).send({ msg: error.message });
    }
  });

  app.delete("/areas/:id", async (req, res) => {
    const { id } = req.params;
    const area = await AreaModel.findOne({ _id: id });
    if (area || area.length == 0) {
      const response = await area.deleteOne({ _id: id });
      res.status(202).json(response);
    } else {
      res.status(404).json({ message: `User not found...` });
    }
  });
};
